-- begin TRAINING_CITY
create table TRAINING_CITY (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(100) not null,
    IS_DEFAULT boolean,
    CODE varchar(20) not null,
    --
    primary key (ID)
)^
-- end TRAINING_CITY
-- begin TRAINING_CUSTOMER
create table TRAINING_CUSTOMER (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    DTYPE varchar(31),
    --
    NAME varchar(100) not null,
    PHONE varchar(20) not null,
    EMAIL varchar(50) not null,
    --
    primary key (ID)
)^
-- end TRAINING_CUSTOMER
-- begin TRAINING_INDIVIDUAL
create table TRAINING_INDIVIDUAL (
    ID varchar(36) not null,
    --
    PASSPORT_NO varchar(20) not null,
    --
    primary key (ID)
)^
-- end TRAINING_INDIVIDUAL
-- begin TRAINING_COMPANY
create table TRAINING_COMPANY (
    ID varchar(36) not null,
    --
    INN varchar(40) not null,
    --
    primary key (ID)
)^
-- end TRAINING_COMPANY
-- begin TRAINING_CAR_SERVICE_CENTER
create table TRAINING_CAR_SERVICE_CENTER (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(100) not null,
    CREATOR_ID varchar(36),
    PHONE varchar(50) not null,
    CITY_ID varchar(36) not null,
    ADDRESS varchar(255) not null,
    --
    primary key (ID)
)^
-- end TRAINING_CAR_SERVICE_CENTER
-- begin TRAINING_EMPLOYEE
create table TRAINING_EMPLOYEE (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    FIRST_NAME varchar(100) not null,
    LAST_NAME varchar(100) not null,
    BIRTH_DATE date,
    EMAIL varchar(50),
    SALARY decimal(19, 2),
    CENTER_ID varchar(36) not null,
    --
    primary key (ID)
)^
-- end TRAINING_EMPLOYEE
-- begin TRAINING_REPAIR
create table TRAINING_REPAIR (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    DESCRIPTION varchar(255) not null,
    CENTER_ID varchar(36) not null,
    EMPLOYEE_ID varchar(36) not null,
    --
    primary key (ID)
)^
-- end TRAINING_REPAIR
-- begin TRAINING_CAR_SERVICE_CENTER_CUSTOMER_LINK
create table TRAINING_CAR_SERVICE_CENTER_CUSTOMER_LINK (
    CAR_SERVICE_CENTER_ID varchar(36) not null,
    CUSTOMER_ID varchar(36) not null,
    primary key (CAR_SERVICE_CENTER_ID, CUSTOMER_ID)
)^
-- end TRAINING_CAR_SERVICE_CENTER_CUSTOMER_LINK
