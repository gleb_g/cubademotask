-- HSQLSB setting to make multi-column UNIQUE constraints properly work with null values in DELETE_TS fields
SET DATABASE SQL UNIQUE NULLS FALSE