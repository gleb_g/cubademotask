alter table TRAINING_COMPANY alter column INN rename to INN__U55566 ^
alter table TRAINING_COMPANY alter column INN__U55566 set null ;
alter table TRAINING_COMPANY add column INN varchar(40) ^
update TRAINING_COMPANY set INN = '' where INN is null ;
alter table TRAINING_COMPANY alter column INN set not null ;
