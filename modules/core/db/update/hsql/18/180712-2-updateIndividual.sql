alter table TRAINING_INDIVIDUAL alter column PASSPORT_NO rename to PASSPORT_NO__U45669 ^
alter table TRAINING_INDIVIDUAL alter column PASSPORT_NO__U45669 set null ;
alter table TRAINING_INDIVIDUAL add column PASSPORT_NO varchar(20) ^
update TRAINING_INDIVIDUAL set PASSPORT_NO = '' where PASSPORT_NO is null ;
alter table TRAINING_INDIVIDUAL alter column PASSPORT_NO set not null ;
