package com.company.training

import com.company.training.entity.Employee
import com.haulmont.cuba.core.global.AppBeans
import com.haulmont.cuba.core.global.TimeSource

Employee employee = emp

def age = AppBeans.get(TimeSource.class).currentTimestamp().toCalendar().get(Calendar.YEAR) - employee.birthDate.toCalendar().get(Calendar.YEAR)

return "Поздравляем вас с днем рождения, уважаемый ${employee.firstName} ${employee.lastName}!\n" +
        "Желаем всего наилучшего в ваши ${age} лет!\n" +
        "С уважением, коллектив автосервиса \"${employee.center.name}\"."
