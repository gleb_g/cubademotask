package com.company.training.conf;

import com.haulmont.cuba.core.config.Config;
import com.haulmont.cuba.core.config.Property;
import com.haulmont.cuba.core.config.Source;
import com.haulmont.cuba.core.config.SourceType;
import com.haulmont.cuba.core.config.defaults.Default;

@Source(type = SourceType.DATABASE)
public interface EmailConfig extends Config {

    @Property("training.email.from")
    @Default("test@haulmont.com")
    String getEmailFrom();
}
