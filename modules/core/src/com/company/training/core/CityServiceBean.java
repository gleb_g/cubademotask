package com.company.training.core;

import com.company.training.entity.City;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import com.haulmont.cuba.core.global.AppBeans;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

@Service(CityService.NAME)
public class CityServiceBean implements CityService {
    @Inject
    private Persistence persistence;

    @Transactional
    @Override
    public City getDefaultCity() {
        Query query = persistence.getEntityManager().createQuery(
                "select c from training$City c where c.isDefault = true");
        return (City) query.getFirstResult();
    }

    @Transactional
    @Override
    public void resetDefaultCity(City newDefaultCity) {
        EntityManager entityManager = persistence.getEntityManager();
        AppBeans.get(DefaultCityCleaner.class).clean(newDefaultCity, entityManager);
    }
}
