package com.company.training.core;

import com.company.training.entity.City;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Query;
import org.springframework.stereotype.Component;

@Component
public class DefaultCityCleaner {
    public void clean(City entity, EntityManager entityManager) {
        Query query = entityManager.createQuery(
                "update training$City c set c.isDefault = false where c.id <> :id");
        query.setParameter("id", entity.getId());
        query.executeUpdate();
    }
}
