package com.company.training.listener;

import com.haulmont.cuba.core.global.UserSessionSource;
import org.springframework.stereotype.Component;
import com.haulmont.cuba.core.listener.BeforeInsertEntityListener;
import com.haulmont.cuba.core.EntityManager;
import com.company.training.entity.CarServiceCenter;

import javax.inject.Inject;

@Component("training_CarServiceCenterEntityListener")
public class CarServiceCenterEntityListener implements BeforeInsertEntityListener<CarServiceCenter> {

    @Inject
    private UserSessionSource userSessionSource;

    @Override
    public void onBeforeInsert(CarServiceCenter entity, EntityManager entityManager) {
        entity.setCreator(userSessionSource.getUserSession().getUser());
    }
}