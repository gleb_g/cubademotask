package com.company.training.listener;

import com.company.training.core.DefaultCityCleaner;
import com.company.training.entity.City;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.listener.BeforeInsertEntityListener;
import com.haulmont.cuba.core.listener.BeforeUpdateEntityListener;
import org.springframework.stereotype.Component;

@Component("training_CityEntityListener")
public class CityEntityListener implements BeforeInsertEntityListener<City>, BeforeUpdateEntityListener<City> {

    @Override
    public void onBeforeInsert(City entity, EntityManager entityManager) {
        clearDefaultFlag(entity, entityManager);
    }

    @Override
    public void onBeforeUpdate(City entity, EntityManager entityManager) {
        clearDefaultFlag(entity, entityManager);
    }

    private void clearDefaultFlag(City entity, EntityManager entityManager) {
        AppBeans.get(DefaultCityCleaner.class).clean(entity, entityManager);
    }
}