package com.company.training.mbeans;

import com.company.training.conf.EmailConfig;
import com.company.training.entity.Employee;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import com.haulmont.cuba.core.app.EmailerAPI;
import com.haulmont.cuba.core.global.Configuration;
import com.haulmont.cuba.core.global.EmailException;
import com.haulmont.cuba.core.global.EmailInfo;
import com.haulmont.cuba.core.global.Scripting;
import groovy.lang.Binding;
import org.apache.commons.lang.ObjectUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;

@Component("training_EmployeeWorkerMBean")
public class EmployeeWorker implements EmployeeWorkerMBean {
    @Inject
    private Persistence persistence;

    @Inject
    private Scripting scripting;

    @Inject
    private EmailerAPI emailerAPI;

    @Inject
    protected Configuration configuration;

    @Transactional
    @Override
    public void sendGreetings() {
        List<Employee> employees = findBirthdayPeople();
        for (Employee employee: employees) {
            Binding binding = new Binding();
            binding.setVariable("emp", employee);
            Object message = scripting.runGroovyScript("com/company/training/buildGreetingMessage.groovy", binding);

            EmailInfo emailInfo = new EmailInfo(
                    employee.getEmail(),
                    "Happy birthday!",
                    configuration.getConfig(EmailConfig.class).getEmailFrom(),
                    ObjectUtils.toString(message), null);
            try {
                emailerAPI.sendEmail(emailInfo);
            } catch (EmailException e) {
                e.printStackTrace();
            }
        }
    }

    private List<Employee> findBirthdayPeople() {
        EntityManager entityManager = persistence.getEntityManager();
        Query query = entityManager.createQuery(
                "select e from training$Employee e " +
                        "where extract(day from e.birthDate) = extract(day from CURRENT_DATE) " +
                        "and extract(month from e.birthDate) = extract(month from CURRENT_DATE)");
        return query.getResultList();
    }
}
