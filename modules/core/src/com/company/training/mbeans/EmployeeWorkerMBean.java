package com.company.training.mbeans;

import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

@ManagedResource(description = "Performs operations on employees")
public interface EmployeeWorkerMBean {
    @ManagedOperation(description = "Sends greeting emails")
    void sendGreetings();
}
