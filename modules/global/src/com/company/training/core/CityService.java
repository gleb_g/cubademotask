package com.company.training.core;

import com.company.training.entity.City;

public interface CityService {
    String NAME = "training_CityService";

    City getDefaultCity();

    void resetDefaultCity(City newDefaultCity);
}
