package com.company.training.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.List;
import javax.persistence.OneToMany;
import com.haulmont.chile.core.annotations.Composition;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.entity.annotation.OnDeleteInverse;
import com.haulmont.cuba.core.global.DeletePolicy;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import com.haulmont.cuba.security.entity.User;
import com.haulmont.cuba.core.entity.annotation.Listeners;

@Listeners("training_CarServiceCenterEntityListener")
@NamePattern("%s|name")
@Table(name = "TRAINING_CAR_SERVICE_CENTER")
@Entity(name = "training$CarServiceCenter")
public class CarServiceCenter extends StandardEntity {
    private static final long serialVersionUID = -8938399445776565923L;

    @NotNull
    @Column(name = "NAME", nullable = false, length = 100)
    protected String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CREATOR_ID")
    protected User creator;

    @NotNull
    @Column(name = "PHONE", nullable = false, length = 50)
    protected String phone;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "CITY_ID")
    @OnDeleteInverse(DeletePolicy.DENY)
    protected City city;

    @NotNull
    @Column(name = "ADDRESS", nullable = false)
    protected String address;

    @OneToMany(mappedBy = "center")
    protected List<Employee> employees;

    @Composition
    @OnDelete(DeletePolicy.CASCADE)
    @OneToMany(mappedBy = "center")
    protected List<Repair> repairs;

    @JoinTable(name = "TRAINING_CAR_SERVICE_CENTER_CUSTOMER_LINK",
        joinColumns = @JoinColumn(name = "CAR_SERVICE_CENTER_ID"),
        inverseJoinColumns = @JoinColumn(name = "CUSTOMER_ID"))
    @ManyToMany
    protected List<Customer> clients;

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public User getCreator() {
        return creator;
    }


    public void setClients(List<Customer> clients) {
        this.clients = clients;
    }

    public List<Customer> getClients() {
        return clients;
    }


    public void setRepairs(List<Repair> repairs) {
        this.repairs = repairs;
    }

    public List<Repair> getRepairs() {
        return repairs;
    }


    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Employee> getEmployees() {
        return employees;
    }


    public void setCity(City city) {
        this.city = city;
    }

    public City getCity() {
        return city;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }


}