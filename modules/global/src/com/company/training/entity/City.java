package com.company.training.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.annotation.Listeners;
import javax.persistence.UniqueConstraint;

@Listeners("training_CityEntityListener")
@NamePattern("%s|name")
@Table(name = "TRAINING_CITY", uniqueConstraints = {
    @UniqueConstraint(name = "IDX_TRAINING_CITY_UNQ", columnNames = {"NAME", "DELETE_TS"})
})
@Entity(name = "training$City")
public class City extends StandardEntity {
    private static final long serialVersionUID = -2351902398809126301L;

    @NotNull
    @Column(name = "NAME", nullable = false, length = 100)
    protected String name;

    @Column(name = "IS_DEFAULT")
    protected Boolean isDefault;

    @NotNull
    @Column(name = "CODE", nullable = false, length = 20)
    protected String code;


    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }


}