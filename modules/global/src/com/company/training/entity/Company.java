package com.company.training.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.persistence.DiscriminatorValue;

@DiscriminatorValue("M")
@PrimaryKeyJoinColumn(name = "ID", referencedColumnName = "ID")
@Table(name = "TRAINING_COMPANY")
@Entity(name = "training$Company")
public class Company extends Customer {
    private static final long serialVersionUID = -7165392232896449506L;

    @NotNull
    @Column(name = "INN", nullable = false, length = 40)
    protected String inn;

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }



}