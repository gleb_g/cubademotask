package com.company.training.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.PrimaryKeyJoinColumn;
import com.haulmont.chile.core.annotations.NumberFormat;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.persistence.DiscriminatorValue;

@DiscriminatorValue("I")
@PrimaryKeyJoinColumn(name = "ID", referencedColumnName = "ID")
@Table(name = "TRAINING_INDIVIDUAL")
@Entity(name = "training$Individual")
public class Individual extends Customer {
    private static final long serialVersionUID = -9206313187454651560L;

    @NotNull
    @Column(name = "PASSPORT_NO", nullable = false, length = 20)
    protected String passportNo;

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }



}