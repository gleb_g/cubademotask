package com.company.training.web.carservicecenter;

import com.company.training.core.CityService;
import com.company.training.entity.Company;
import com.company.training.entity.Customer;
import com.company.training.entity.Individual;
import com.haulmont.cuba.core.global.Messages;
import com.haulmont.cuba.gui.components.AbstractEditor;
import com.company.training.entity.CarServiceCenter;
import com.haulmont.cuba.gui.components.TabSheet;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.gui.data.CollectionDatasource;

import javax.inject.Inject;
import java.util.Map;
import java.util.UUID;

public class CarServiceCenterEdit extends AbstractEditor<CarServiceCenter> {

    @Inject
    private CityService cityService;

    @Inject
    protected TabSheet tabsheet;

    @Inject
    private CollectionDatasource<Customer, UUID> clientsDs;

    @Inject
    protected Messages messages;

    @Inject
    private Table<Customer> clientsTable;

    @Override
    protected void initNewItem(CarServiceCenter item) {
        item.setCity(cityService.getDefaultCity());
    }

    @Override
    public void init(Map<String, Object> params) {
        super.init(params);
        clientsDs.addCollectionChangeListener(e -> {
            TabSheet.Tab clientsTab = tabsheet.getTab("clientsTab");
            if (clientsDs.size() > 0) {
                clientsTab.setCaption("Clients (" + clientsDs.size() + ")");
            } else {
                clientsTab.setCaption("Clients");
            }
        });
        clientsTable.addGeneratedColumn("clientType", entity -> {
            String value = "";
            if (entity.getClass().equals(Individual.class)) {
                value = messages.getMessage(getClass(), "client.individual");
            } else if (entity.getClass().equals(Company.class)) {
                value = messages.getMessage(getClass(), "client.company");
            }
            return new Table.PlainTextCell(value);
        });
        clientsTable.getColumn("clientType").setCaption(messages.getMessage(getClass(), "clientType"));
    }
}