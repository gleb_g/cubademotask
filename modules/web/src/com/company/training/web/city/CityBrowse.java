package com.company.training.web.city;

import com.haulmont.cuba.gui.components.AbstractLookup;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.gui.components.Window;
import com.haulmont.cuba.gui.components.actions.CreateAction;
import com.haulmont.cuba.gui.components.actions.EditAction;

import java.util.Map;

public class CityBrowse extends AbstractLookup {
    @Override
    public void init(Map<String, Object> params) {
        Table citiesTable = (Table) getComponent("citiesTable");
        if (citiesTable != null) {
            citiesTable.addAction(new EditAction(citiesTable) {
                @Override
                protected void afterWindowClosed(Window window) {
                    getTarget().getDatasource().refresh();
                    super.afterWindowClosed(window);
                }
            });
            citiesTable.addAction(new CreateAction(citiesTable) {
                @Override
                protected void afterWindowClosed(Window window) {
                    getTarget().getDatasource().refresh();
                    super.afterWindowClosed(window);
                }
            });
        }
    }
}