package com.company.training.web.city;

import com.company.training.core.CityService;
import com.haulmont.cuba.core.app.DataService;
import com.haulmont.cuba.core.global.*;
import com.haulmont.cuba.gui.components.AbstractEditor;
import com.company.training.entity.City;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

public class CityEdit extends AbstractEditor<City> {

    @Inject
    private CityService cityService;

    @Inject
    private DataManager dataManager;

    @Override
    protected boolean postCommit(boolean committed, boolean close) {
        if (committed) {
//            FIRST APPROACH

//            cityService.resetDefaultCity(getItem());

//            SECOND APPROACH

//            List<City> cities = dataManager.load(City.class)
//                    .query("select c from training$City c where c.id <> :id")
//                    .parameter("id", getItem().getId())
//                    .list();
//            cities.forEach(city -> city.setIsDefault(false));
//            dataManager.commit(new CommitContext(cities));


//             OR

//            DataService dataService = AppBeans.get(DataService.class);
//            LoadContext<City> loadContext = new LoadContext(City.class);
//            loadContext.setQueryString("select c from training$City c where c.id <> :id")
//                    .setParameter("id", getItem().getId());
//            List<City> list = dataService.loadList(loadContext);
//
//            list.forEach(city -> city.setIsDefault(false));
//
//            dataService.commit(new CommitContext(list));

        }
        return super.postCommit(committed, close);
    }


}