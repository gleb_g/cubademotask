package com.company.training.web.repair;

import com.haulmont.cuba.gui.components.AbstractEditor;
import com.company.training.entity.Repair;
import com.haulmont.cuba.gui.data.Datasource;

import javax.inject.Inject;
import java.util.Map;
import java.util.UUID;

public class RepairEdit extends AbstractEditor<Repair> {

    @Inject
    private Datasource<Repair> repairDs;

    @Override
    public void init(Map<String, Object> params) {
        super.init(params);
        repairDs.addItemPropertyChangeListener(e -> {
            if ("center".equals(e.getProperty()))
                getItem().setEmployee(null);
        });
    }
}